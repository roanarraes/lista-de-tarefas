import axios from 'axios';

const api = axios.create({
  baseURL:`http://ldt-api.herokuapp.com`
});

export default api;
