import './styles.css';

import React, { Component } from "react";

import TableToDo from '../../components/TableToDo';
import ButtonCreateToDo from '../../components/ButtonCreateTask';
import InputField from '../../components/InputField';
import SelectField from '../../components/SelectField';

import { MdClose } from "react-icons/md";

import api from '../../services/api';





const initialState = {
  showModal:false,
  titleModal:"",
  buttonCreateOrEdit:"",
  enableButtonEdit:false,
  statusTaskApi:"Todas",
  checkedRadioTodas:true,
  checkedRadioPendente:false,
  checkedRadioEmAndamento:false,
  checkedRadioConcluida:false,
  idTask:"",
  descriptionTask:"",
  titleTask:"",
  responsibleTask:"",
  dateToDoneTask:"",
  statusTask:"",
  tasksToDo:""
}

class Main extends Component{

  constructor(props){
    super(props);
    this.state = initialState;
    this.handleClickModal             = this.handleClickModal.bind(this);
    this.handleCreateOrEdit           = this.handleCreateOrEdit.bind(this);
    this.handleChangeTitleTask        = this.handleChangeTitleTask.bind(this);
    this.handleChangeDescriptionTask  = this.handleChangeDescriptionTask.bind(this);
    this.handleChangeResponsibleTask  = this.handleChangeResponsibleTask.bind(this);
    this.handleChangeDateToDoneTask   = this.handleChangeDateToDoneTask.bind(this);
    this.handleChangeStatus           = this.handleChangeStatus.bind(this);
    this.handleClearCreateTasks       = this.handleClearCreateTasks.bind(this);
    this.handleEditTask               = this.handleEditTask.bind(this);
    this.handleDoneTask               = this.handleDoneTask.bind(this);
    this.handleDeleteTask             = this.handleDeleteTask.bind(this);
    this.handleLoadTaskFilter         = this.handleLoadTaskFilter.bind(this);
    this.loadtodolistApi              = this.loadtodolistApi.bind(this);
    this.editTaskApi                  = this.editTaskApi.bind(this);
    this.createTaskApi                = this.createTaskApi.bind(this);
    this.deleteTaskApi                = this.deleteTaskApi.bind(this);
    this.doneTaskApi                  = this.doneTaskApi.bind(this);
    this.loadTasksFilter              = this.loadTasksFilter.bind(this);
  }

  componentDidMount(){
    this.loadtodolistApi();
  }

  handleClickModal() {
      this.setState({ buttonCreateOrEdit:"Criar",
                      showModal:true,
                      titleModal:"Criar Nova Tarefa",
                      enableButtonEdit:false,
                      idTask:"",
                      titleTask:"",
                      descriptionTask:"",
                      responsibleTask:"",
                      dateToDoneTask:"",
                      statusTask:"Pendente"
                    });
  }

  handleChangeTitleTask(e){this.setState({titleTask:e.target.value})}
  handleChangeDescriptionTask(e){this.setState({descriptionTask:e.target.value})}
  handleChangeResponsibleTask(e){this.setState({responsibleTask:e.target.value})}
  handleChangeDateToDoneTask(e){this.setState({dateToDoneTask:e.target.value})}
  handleChangeStatus(e){this.setState({statusTask:e.target.value})}

  handleCreateOrEdit(e){
    if(this.state.enableButtonEdit){
      this.setState({showModal:false});
      this.editTaskApi();
    }
    else {
    this.setState({showModal:false});
    this.createTaskApi();
    }
  }

  handleClearCreateTasks(){
    if(this.state.enableButtonEdit === false)
    {
      this.setState({
        idTask:"",
        titleTask:"",
        descriptionTask:"",
        responsibleTask:"",
        dateToDoneTask:"",
        statusTask:"Pendente"})
    }
    else return
  }

  handleEditTask(e){
    this.setState({ showModal:true,
                    titleModal:"Editar Tarefa",
                    buttonCreateOrEdit:"Editar",
                    enableButtonEdit:true,
                    idTask:this.state.tasksToDo[e.currentTarget.value].ID,
                    descriptionTask:this.state.tasksToDo[e.currentTarget.value].Descricao,
                    titleTask:this.state.tasksToDo[e.currentTarget.value].Titulo,
                    responsibleTask:this.state.tasksToDo[e.currentTarget.value].Responsavel,
                    dateToDoneTask:this.state.tasksToDo[e.currentTarget.value].DataDeEntrega,
                    statusTask:this.state.tasksToDo[e.currentTarget.value].Status
                  })
  }

  handleDoneTask(e){
    this.doneTaskApi(this.state.tasksToDo[e.currentTarget.value].ID)
  }

  handleDeleteTask(e){
    this.deleteTaskApi(e);
  }

  handleCloseForm(){
    this.setState({showModal:false});
  }

  handleLoadTaskFilter(e){
    let status = e.currentTarget.value;
    this.setState({statusTaskApi:status})
    this.loadTasksFilter(status);
  }

  loadtodolistApi = async() => {
    const response = await api.get(`/tarefas`);
    this.setState({tasksToDo:response.data});
  }

  editTaskApi = async() => {
    await api.patch(`/tarefas/${this.state.idTask}`, {
      titulo:`${this.state.titleTask}`,
      descricao:`${this.state.descriptionTask}`,
      responsavel:`${this.state.responsibleTask}`,
      dataDeEntrega:`${this.state.dateToDoneTask}`,
      status:`${this.state.statusTask}`
    }).then(function(response){
      console.log('editado com sucesso!!');
    })
    .catch(err => console.log(err.response.data));
    if(this.state.statusTaskApi === 'Todas')
    {
      this.loadtodolistApi();
    }else{
      this.loadTasksFilter(this.state.statusTaskApi);
    }
  }

  createTaskApi = async() =>{
    await api.post(`/tarefas`, {
      titulo:`${this.state.titleTask}`,
      descricao:`${this.state.descriptionTask}`,
      responsavel:`${this.state.responsibleTask}`,
      dataDeEntrega:`${this.state.dateToDoneTask}`,
      status:`${this.state.statusTask}`
    }).then(function(response){
      console.log('criado com sucesso!!');
    })
    .catch(err => console.log(err.response.data));
    this.loadtodolistApi();
  }

  deleteTaskApi = async(e) =>{
    await api.delete(`/tarefas/${this.state.tasksToDo[e.currentTarget.value].ID}`)
      .then(function(response){
        console.log('deletado com sucesso!!');
    })
    .catch(err => console.log(err.response.data));
    if(this.state.statusTaskApi === 'Todas')
    {
      this.loadtodolistApi();
    }else{
      this.loadTasksFilter(this.state.statusTaskApi);
    }
  }

  doneTaskApi = async(id) => {
    await api.patch(`/tarefas/concluida/${id}`, {
      status:'Concluída'
    }).then(function(response){
      console.log('editado com sucesso!!');
    })
    .catch(err => console.log(err.response.data));

    if(this.state.statusTaskApi === 'Todas')
    {
      this.loadtodolistApi();
    }else{
      this.loadTasksFilter(this.state.statusTaskApi);
    }
  }


  loadTasksFilter = async(status) => {
    if(status === 'Todas'){
      this.loadtodolistApi()
    }
    else {
      const response = await api.get(`/tarefas/status/${status}`);
      this.setState({tasksToDo:response.data});
    }
  }


  render(){
    return(
      <div className="container main">
        <div className="button-create-to-do">
          <ButtonCreateToDo onClick={()=> this.handleClickModal()} />
        </div>
        <div className="title-status-radio"><span>Filtrar por Status</span></div>
        <div className="div-input-radio">
          <div><input type="radio" name="status" value="Todas" defaultChecked={this.state.checkedRadioTodas}
                      onClick={(e) => this.handleLoadTaskFilter(e)} /><span className="span-input-radio">Todas</span></div>
          <div><input type="radio" name="status" value="Pendente" defaultChecked={this.state.checkedRadioPendente}
                      onClick={(e) => this.handleLoadTaskFilter(e)} /><span className="span-input-radio">Pendente</span></div>
          <div><input type="radio" name="status" value="Em Andamento" defaultChecked={this.state.checkedRadioEmAndamento}
                      onClick={(e) => this.handleLoadTaskFilter(e)} /><span className="span-input-radio">Em Andamento</span></div>
          <div><input type="radio" name="status" value="Concluida" defaultChecked={this.state.checkedRadioConcluida}
                      onClick={(e) => this.handleLoadTaskFilter(e)} /><span className="span-input-radio">Concluída</span></div>
        </div>



        <div className="div-components">
          {this.state.showModal ?
            <div className="form">
              <button title="Fechar" className="btn-close-form" onClick={() => this.handleCloseForm()}><MdClose /></button>
              <span className="title-form">{this.state.titleModal}</span>
              <InputField type="text" value={this.state.titleTask} maxlength="150"
                            onChange={(e)=> this.handleChangeTitleTask(e)}
                            text="Título" className="form-control input-text" alt="editar" />
              <InputField type="text" value={this.state.descriptionTask} maxlength="255"
                            onChange={(e)=> this.handleChangeDescriptionTask(e)}
                            text="Descrição" className="form-control input-text" />
              <InputField type="text" value={this.state.responsibleTask} maxlength="60"
                            onChange={(e)=> this.handleChangeResponsibleTask(e)}
                            text="Responsável" className="form-control input-text" />
              <InputField type="text" value={this.state.dateToDoneTask} maxlength="10"
                            onChange={(e)=> this.handleChangeDateToDoneTask(e)}
                            text="Data De Entrega" className="form-control input-text" />
              <SelectField text="Status" className="input-text" value={this.state.statusTask} maxlength="20"
                            onChange={(e)=> this.handleChangeStatus(e)}/>
              <div className="div-form-button">
                <button className="button-form" onClick={(e) => {this.handleCreateOrEdit(e)}}>{this.state.buttonCreateOrEdit}</button>
                <button className="button-form" onClick={() => this.handleClearCreateTasks()}>Limpar</button>
              </div>
            </div>
          : null}
        </div>

        <div className="table-to-do">
          <TableToDo value={this.state.tasksToDo} onClickBtnEdit={(e) => this.handleEditTask(e)}
                                                  onClickBtnDone={(e) => this.handleDoneTask(e)}
                                                  onClickBtnDelete={(e) => this.handleDeleteTask(e)} />
        </div>
      </div>
    );
  };
}

export default Main;
