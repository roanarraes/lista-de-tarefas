import './style.css';

import React, {Component} from 'react';

class SelectField extends Component{
render(){
  return(
    <div className="div-input">
      <span>{this.props.text}</span>
      <select className="browser-default custom-select" onChange={this.props.onChange} value={this.props.value}>
        <option className="select-form-option" value="Pendente">Pendente</option>
        <option value="Em Andamento">Em Andamento</option>
        <option value="Concluída">Concluída</option>
      </select>
    </div>
  );
}
}
export default SelectField;

