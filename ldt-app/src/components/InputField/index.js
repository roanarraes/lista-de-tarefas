import './styles.css';

import React, {Component} from 'react';

class InputField extends Component{
  render(){
    return(
      <div className="div-input">
        <span>{this.props.text}</span>
        <input type={this.props.type} className={this.props.className} value={this.props.value}
                placeholder={this.props.placeholder} onChange={this.props.onChange} maxLength={this.props.maxlength}>
        </input>
      </div>
    );
  }
}
export default InputField;
