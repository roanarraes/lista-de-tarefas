import './styles.css';

import React, {Component} from 'react';

import { Table } from 'reactstrap';
import { MdClose } from "react-icons/md";
import { MdDone } from 'react-icons/md'
import { MdEdit } from 'react-icons/md'

class TableToDo extends Component{

  renderOptions(){

    if(this.props.value === null){
      return null;
    }

      return Object.values(this.props.value).map((value,index) =>{
        return (
          <tr key={index}>
            <td>{value.Titulo}</td>
            <td>{value.Descricao}</td>
            <td>{value.Responsavel}</td>
            <td>{value.DataDeEntrega}</td>
            <td>{value.Status}</td>
            <td><button title="Editar" className="btn-edit-table" value={index} onClick={this.props.onClickBtnEdit}><MdEdit /></button></td>
            <td><button title="Concluir" className="btn-done-table" value={index} onClick={this.props.onClickBtnDone}><MdDone /></button></td>
            <td><button title="Excluir" className="btn-close-table"value={index} onClick={this.props.onClickBtnDelete}><MdClose /></button></td>
          </tr>
        )
      })
  }

  render(){
    return(
      <Table responsive hover>
        <thead>
          <tr>
            <th>Título</th>
            <th>Descrição</th>
            <th>Responsável</th>
            <th>Data de Entrega</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
            {this.renderOptions()}
        </tbody>
      </Table>
    );
  }
}

export default TableToDo;
