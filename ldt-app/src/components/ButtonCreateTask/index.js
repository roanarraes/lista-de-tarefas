import './styles.css';

import React, {Component} from 'react';
import { Button } from 'reactstrap';

class ButtonCreateTask extends Component {
  render(){
    return (
      <Button onClick={this.props.onClick} outline color="primary">Criar Tarefa</Button>
    );
  }
}

export default ButtonCreateTask;