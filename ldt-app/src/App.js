import './styles.css';

import React from 'react';

import Main from '../src/pages/main/index'
import Header from './components/Header';

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
    </div>
  );
}

export default App;
