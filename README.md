Lista de Tarefas simples


- Aplicacao rodando no heroku nos seguintes links:

Front-end : http://ldt-app.herokuapp.com/
Back-end : http://ldt-api.herokuapp.com/

- Banco de dados externo configurado, via clearDB MySQL (Heroku).
- Para criar tabela, acessar ldt-api/mysql/create-table e executar node create-table.
- Para popular a tabela, acessar ldt-api/mysql/seeds-table e executar node seeds-table.


Funçoes da API:

Back-end : http://ldt-api.herokuapp.com/  - (get retorna mensagem)
Back-end : http://ldt-api.herokuapp.com/tarefas - (get retorna todas as tarefas da tabela).
Back-end : http://ldt-api.herokuapp.com/tarefas/idDaTarefa - (patch enviando informações para editar uma tarefa).
Back-end : http://ldt-api.herokuapp.com/tarefas - (post enviando informações para a criação da tabela) - Cria nova tarefa.
Back-end : http://ldt-api.herokuapp.com/tarefas/idDaTarefa - (delete) - Apaga uma tarefa especifica.
Back-end : http://ldt-api.herokuapp.com/tarefas/concluida/idDaTarefa - (patch) - seta uma tarefa especifica como concluida.
Back-end : http://ldt-api.herokuapp.com/tarefas/status/status (get) - retorna lista de tareas conforme status.


Executar API Local:
- Acessar ldt-api e executar node index.js.

Executar APP Local:
- Acessar ldt-app e executar npm start.