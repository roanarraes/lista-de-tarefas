const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : 'us-cdbr-iron-east-05.cleardb.net',
  user     : 'bb0f609afaba35',
  password : '11c78b15',
  database : 'heroku_8d739f2459c2e88'
});

connection.connect(function(err){
  if(err) return console.log(err);
  console.log('conectou!');
  addRows(connection);
})

function addRows(conn){
  const sql = "INSERT INTO Tarefas(Titulo,Descricao,Responsavel,DataDeEntrega,Status) VALUES ?";
  const values = [
    ['Fazer Cafe','Fazer cafe para eu mesmo','Roan Arraes','01/01/2020','Pendente'],
    ['Almoçar','Almoçar na esquina','Roan Arraes','01/01/2020','Pendente'],
    ['Café da tarde','Café da tarde na esquina','Roan Arraes','01/03/2021','Pendente'],
    ['Jantar','Jantar em casa','Roan Arraes','02/05/2030','Pendente']
  ];

  conn.query(sql, [values], function(error, results, fields){
    if(error) return console.log(error);
    console.log('adcionou o(s) registro(s)');
    connection.end();
  })
}