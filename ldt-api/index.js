const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3003;
const mysql = require('mysql');
const allowCors = require('./src/config/cors');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCors);

const router = express.Router();
router.get('/', (req, res) => res.json({ message: 'Opa!!' }));
app.use('/', router);

app.listen(port);
console.log(`API funcionando! (porta:${port})`);


router.get('/tarefas', (req, res) =>{
  execSQLQuery('SELECT * FROM Tarefas', res);
})

router.get('/tarefas/:id?', (req, res) =>{
  let filter = '';
  if(req.params.id){ 
  const consulta = 'SELECT * FROM Tarefas WHERE ID=?';
  execSQLQuery(consulta, res, parseInt(req.params.id));
}
})

router.get('/tarefas/status/:status?', (req, res) =>{
  if(req.params.status){ 
  const consulta = 'SELECT * FROM Tarefas WHERE STATUS=?';
  execSQLQuery(consulta, res, req.params.status);
}
})

router.delete('/tarefas/:id', (req, res) =>{
  execSQLQuery('DELETE FROM Tarefas WHERE ID=' + parseInt(req.params.id), res);
})

router.post('/tarefas', (req, res) =>{
    const titulo = req.body.titulo.substring(0,150);
    const descricao = req.body.descricao.substring(0,255);
    const responsavel = req.body.responsavel.substring(0,60);
    const dataDeEntrega = req.body.dataDeEntrega.substring(0,10);
    const status = req.body.status.substring(0,20) ;
    execSQLQuery(`INSERT INTO Tarefas(Titulo,Descricao,Responsavel,DataDeEntrega,Status) 
                  VALUES('${titulo}','${descricao}','${responsavel}','${dataDeEntrega}',
                          '${status}')`, res);
});


router.patch('/tarefas/:id', (req, res) =>{

  console.log(req.body)

  const id = parseInt(req.params.id);
  const titulo = req.body.titulo.substring(0,150);
  const descricao = req.body.descricao.substring(0,255);
  const responsavel = req.body.responsavel.substring(0,60);
  const dataDeEntrega = req.body.dataDeEntrega.substring(0,10);
  const status = req.body.status.substring(0,20) ;
  execSQLQuery(`UPDATE Tarefas SET Titulo='${titulo}',Descricao='${descricao}',Responsavel='${responsavel}',
                  DataDeEntrega='${dataDeEntrega}',Status='${status}' WHERE ID=${id}`, res);
})

router.patch('/tarefas/concluida/:id', (req, res) =>{

  console.log(req.body)

  const id = parseInt(req.params.id);
  const status = req.body.status.substring(0,20) ;
  execSQLQuery(`UPDATE Tarefas SET Status='${status}' WHERE ID=${id}`, res);
})


const execSQLQuery = (sqlQry, res, id) =>{
  const connection = mysql.createConnection({
    host     : 'us-cdbr-iron-east-05.cleardb.net',
    user     : 'bb0f609afaba35',
    password : '11c78b15',
    database : 'heroku_8d739f2459c2e88',
  });
 
  connection.query(sqlQry,[id],(error, results, fields) =>{
      if(error) 
        res.json(error);
      else
        res.json(results);
        console.log('executou!');
      connection.end();
  });
}


